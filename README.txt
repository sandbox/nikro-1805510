About the Module
================
This modules offers dynamic beautytip tooltips to be used inside of the views.

It duplicates existing views field handlers, user handlers and node handlers,
extends them and uses the extended version to create tooltips.

Inside the Tooltip handlers you can specify Replacement Tokens (which will build the
tooltip dynamically), and some other beautytips configurations.


Supported Fields
===========================
This module supports the following fields:
+ Basic Node fields
+ Field fields
- Basic User fields (Not yet)
- Basic Taxonomy fields (Not yet)


Author & Contacts
=================
Nikro
Site: http://nikro.me/
Drupal User: http://drupal.org/user/576416
