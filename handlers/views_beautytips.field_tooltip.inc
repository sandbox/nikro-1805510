<?php
/**
 * @file
 * This file contains an extended field views handler
 */

/**
 * This handler extends from views_handler_field_field and adds tooltip functionality.
 */
class views_handler_field_field_tooltip extends views_handler_field_field {

  /**
   * We add our options to default field's settings form.
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['tooltips'] = array(
      '#type' => 'fieldset',
      '#title' => t('Tooltips Options'),
      '#collapsed' => TRUE,
      '#collapsible' => TRUE,
    );

    $form['tooltips']['create_tooltip'] = array(
      '#type' => 'checkbox',
      '#title' => t('Create tooltip on mouse hover'),
      '#description' => t('Check this box if you want to create a tooltip on mouse hover'),
      '#default_value' => $this->options['create_tooltip'],
      '#weight' => 0,
    );

    $form['tooltips']['tooltip_text'] = array(
      '#type' => 'textarea',
      '#title' => t('Tooltip body'),
      '#description' => t('Use HTML in combination with the tokens provided underneath to create needed tooltip body.'),
      '#default_value' => $this->options['tooltip_text'],
      '#weight' => 1,
      '#dependency' => array(
        'edit-options-tooltips-create-tooltip' => array(1),
      ),
    );

    $form['tooltips']['tooltip_help'] = array(
      '#type' => 'fieldset',
      '#title' => t('Tooltip creation tokens'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#value' => $this->create_rendered_tokens(),
      '#weight' => 2,
      '#dependency' => array(
        'edit-options-tooltips-create-tooltip' => array(1),
      ),
    );

    $form['tooltips']['tooltip_advanced'] = array(
      '#type' => 'fieldset',
      '#title' => t('Advanced options'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#weight' => 3,
      '#dependency' => array(
        'edit-options-tooltips-create-tooltip' => array(1),
      ),
    );

    $form['tooltips']['tooltip_advanced']['info'] = array(
      '#markup' => t('<p><span>For more details read the !documentation</span></p>',
        array('!documentation' => l(t('documentation'), 'http://drupalcode.org/project/beautytips.git/blob/HEAD:/README.txt')),
        array('html' => TRUE)
      ),
      '#weight' => -10,
    );
    $form['tooltips']['tooltip_advanced']['trigger_show'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Triggers to Show'),
      '#description' => t('Specify triggers which will be used to open the tooltip. Defaults to <em>mouseover</em>.'),
      '#options' => $this->get_trigger_options('show'),
      '#default_value' => $this->options['trigger_show'],
      '#weight' => 0,
      '#dependency' => array(
        'edit-options-tooltips-tooltip-advanced-use-hoverintent' => array(0),
      ),
    );

    $form['tooltips']['tooltip_advanced']['trigger_hide'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Triggers to Hide'),
      '#description' => t('Specify triggers which will be used to close the tooltip. Defaults to <em>mouseout</em>.'),
      '#options' => $this->get_trigger_options('hide'),
      '#default_value' => $this->options['trigger_hide'],
      '#weight' => 1,
      '#dependency' => array(
        'edit-options-tooltips-tooltip-advanced-use-hoverintent' => array(0),
      ),
    );

    $form['tooltips']['tooltip_advanced']['use_hoverintent'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use Hover Intent'),
      '#description' => t('If you use this option, the above options will be ignored.'),
      '#default_value' => $this->options['use_hoverintent'],
      '#weight' => 2,
    );

    $form['tooltips']['tooltip_advanced']['hover_intent_interval'] = array(
      '#type' => 'textfield',
      '#title' => t('HoverIntent Interval'),
      '#description' => t('Specify the interval after which the tooltip should appear.'),
      '#default_value' => $this->options['hover_intent_interval'],
      '#weight' => 3,
      '#dependency' => array(
        'edit-options-tooltips-tooltip-advanced-use-hoverintent' => array(1),
      ),
    );

    $form['tooltips']['tooltip_advanced']['hover_intent_timeout'] = array(
      '#type' => 'textfield',
      '#title' => t('HoverIntent Timeout'),
      '#description' => t('Specify the timeout after which the tooltip should disappear.'),
      '#default_value' => $this->options['hover_intent_timeout'],
      '#weight' => 4,
      '#dependency' => array(
        'edit-options-tooltips-tooltip-advanced-use-hoverintent' => array(1),
      ),
    );

    $form['tooltips']['tooltip_advanced']['click_anywhere_to_close'] = array(
      '#type' => 'checkbox',
      '#title' => t('Click anywhere to close'),
      '#description' => t('If users clicks anywhere it will close the tooltip.'),
      '#default_value' => $this->options['click_anywhere_to_close'],
      '#weight' => 5,
    );

    $form['tooltips']['tooltip_advanced']['close_when_others_open'] = array(
      '#type' => 'checkbox',
      '#title' => t('Close when others open'),
      '#description' => t('Close the tooltip when other tooltips open.'),
      '#default_value' => $this->options['close_when_others_open'],
      '#weight' => 6,
    );

    $form['tooltips']['tooltip_advanced']['position_option'] = array(
      '#type' => 'textfield',
      '#title' => t('Toolbox position'),
      '#description' => t('Specify the position of the toolbox. If none is specified then the <em>most</em> value will be used. <br/>Separate values with spaces, i.e. "top bottom left right".'),
      '#default_value' => $this->options['position_option'],
      '#weight' => 7,
    );
  }

  /**
   * Options definitions.
   */
  public function option_definition() {
    $options = parent::option_definition();

    $options['create_tooltip'] = array('default' => FALSE, 'bool' => TRUE);
    $options['tooltip_text'] = array('default' => '');
    $options['trigger_show'] = array('default' => $this->get_trigger_options('show', array('mouseover')));
    $options['trigger_hide'] = array('default' => $this->get_trigger_options('hide', array('mouseout')));

    $options['use_hoverintent'] = array('default' => FALSE, 'bool' => TRUE);
    $options['hover_intent_interval'] = array('default' => '0');
    $options['hover_intent_timeout'] = array('default' => '0');
    $options['click_anywhere_to_close'] = array('default' => TRUE, 'bool' => TRUE);
    $options['close_when_others_open'] = array('default' => FALSE, 'bool' => TRUE);
    $options['position_option'] = array('default' => 'most');

    return $options;
  }

  /**
   * Performs some cleanup tasks on the options array before saving it.
   */
  public function options_submit(&$form, &$form_state) {
    parent::options_submit($form, $form_state);
    $options = &$form_state['values']['options'];

    $options['create_tooltip'] = $options['tooltips']['create_tooltip'];
    $options['tooltip_text'] = $options['tooltips']['tooltip_text'];

    // Advanced options.
    $options['use_hoverintent'] = $options['tooltips']['tooltip_advanced']['use_hoverintent'];
    $options['trigger_show'] = $options['tooltips']['tooltip_advanced']['trigger_show'];
    $options['trigger_hide'] = $options['tooltips']['tooltip_advanced']['trigger_hide'];
    $options['click_anywhere_to_close'] = $options['tooltips']['tooltip_advanced']['click_anywhere_to_close'];
    $options['close_when_others_open'] = $options['tooltips']['tooltip_advanced']['close_when_others_open'];
    $options['position_option'] = $options['tooltips']['tooltip_advanced']['position_option'];

  }

  /**
   * Here we take fully rendered field, with all the altering done in it and tweak it with our tooltip.
   */
  public function advanced_render($values) {
    // Get default output.
    $output = parent::advanced_render($values);

    // Basically we re-use internal replacement.
    if (($this->options['create_tooltip'] == '1') && ($this->options['tooltip_text'] !== '')) {
      $alter_values = array(
        'text' => $this->options['tooltip_text'],
      );
      $tokens = $this->get_render_tokens($alter_values);
      $tooltip_value = $this->render_altered($alter_values, $tokens);

      // Wrap the output in a selector so we will know
      // where the tooltip should be applied.
      $class = array(
        drupal_html_class('tooltip-' . $values->nid),
        drupal_html_class($this->view->name),
        drupal_html_class($this->view->current_display),
        drupal_html_class($this->field),
      );
      $output = '<div class="tooltip-wrapper ' . implode(' ', $class) . '">' . $output . '</div>';

      // We need to use an unique key per node, per field.
      $beautytip_key = implode('_',
        array('views_node_tooltips', $this->field, $values->nid));

      // Create the beautytip option array.
      $bt_options[$beautytip_key] = array(
        'text' => $tooltip_value,
        'cssSelect' => '.' . implode('.', $class),
        'clickAnywhereToClose' => $this->options['click_anywhere_to_close'],
        'closeWhenOthersOpen' => $this->options['close_when_others_open'],
        'position' => $this->options['position_option'],
      );

      // If we have selected hoverIntent, load the library.
      $interval = $this->options['hover_intent_interval'];
      $timeout = $this->options['hover_intent_timeout'];

      if ($this->options['use_hoverintent'] && is_numeric($interval) && ($interval >= 0) && is_numeric($timeout) && ($timeout >= 0)) {
        drupal_add_js(drupal_get_path('module', 'beautytips') . '/other_libs/jquery.hoverIntent.minified.js', array('group' => JS_LIBRARY));
        $bt_options[$beautytip_key]['hoverIntentOpts'] = array(
          'interval' => $interval,
          'timeout' => $timeout,
        );
      }
      elseif ($this->options['use_hoverintent']) {
        drupal_add_js(drupal_get_path('module', 'beautytips') . '/other_libs/jquery.hoverIntent.minified.js', array('group' => JS_LIBRARY));
        $bt_options[$beautytip_key]['hoverIntentOpts'] = array(
          'interval' => 0,
          'timeout' => 0,
        );
        // Reset the values in options, because they were wrong in 1st place.
        $this->options['hover_intent_interval'] = 0;
        $this->options['hover_intent_timeout'] = 0;
      }
      // Create custom trigger.
      else {
        $show_arr = array();
        foreach ($this->options['trigger_show'] as $key => $value) {

          if ($value !== 0) {
            $show_arr[] = $key;
          }
        }
        $show_string = implode(' ', $show_arr);

        $hide_arr = array();
        foreach ($this->options['trigger_hide'] as $key => $value) {
          if ($value !== 0) {
            $hide_arr[] = $value;
          }
        }
        $hide_string = implode(' ', $hide_arr);

        $bt_options[$beautytip_key]['trigger'] = array(
          '0' => $show_string,
          '1' => $hide_string,
        );
      }

      // Actually create the tooltip.
      beautytips_add_beautytips($bt_options);
    }

    return $output;
  }


  /**
   * This chunk was taken from views_handler_field.inc.
   *
   * We use it separatly so we don't litter in our options_form function.
   */
  public function create_rendered_tokens() {
    // Get a list of the available fields and arguments for token replacement.
    $options = array();
    foreach ($this->view->display_handler->get_handlers('field') as $field => $handler) {
      $options[t('Fields')]["[$field]"] = $handler->ui_name();
      // We only use fields up to (and including) this one.
      if ($field == $this->options['id']) {
        break;
      }
    }
    // This lets us prepare the key as we want it printed.
    $count = 0;
    foreach ($this->view->display_handler->get_handlers('argument') as $arg => $handler) {
      $options[t('Arguments')]['%' . ++$count] = t('@argument title', array('@argument' => $handler->ui_name()));
      $options[t('Arguments')]['!' . $count] = t('@argument input', array('@argument' => $handler->ui_name()));
    }

    $this->document_self_tokens($options[t('Fields')]);

    // Default text (that's fine).
    $output = t('<p>You must add some additional fields to this display before using this field. These fields may be marked as <em>Exclude from display</em> if you prefer. Note that due to rendering order, you cannot use fields that come after this field; if you need a field not listed here, rearrange your fields.</p>');
    // We have some options, so make a list.
    if (!empty($options)) {
      $output = t('<p>The following tokens are available for this field. Note that due to rendering order, you cannot use fields that come after this field; if you need a field not listed here, rearrange your fields.
If you would like to have the characters \'[\' and \']\' please use the html entity codes \'%5B\' or  \'%5D\' or they will get replaced with empty space.</p>');
      foreach (array_keys($options) as $type) {
        if (!empty($options[$type])) {
          $items = array();
          foreach ($options[$type] as $key => $value) {
            $items[] = $key . ' == ' . $value;
          }
          $output .= theme('item_list',
            array(
              'items' => $items,
              'type' => $type,
            ));
        }
      }
    }

    // Ok, now we're fine to return the actual rendered output.
    return $output;
  }

  /**
   * Helper function used to get the options for the trigger checkboxes.
   *
   * @param string $case
   *   This can be either 'show' or 'hide'.
   *
   * @param array $values
   *   If you want to get an array of already selected options, specify this param, i.e. array('focus');
   */
  public function get_trigger_options($case, $values = array()) {
    $items = array();
    if ($case == 'show') {
      $items['mouseover'] = t('Mouse Over');
      $items['focus'] = t('Focus');
      $items['click'] = t('Click');
      $items['dbclick'] = t('Double Click');
      $items['hoverIntent'] = t('Hover Intent (options)');
    }
    elseif ($case == 'hide') {
      $items['mouseout'] = t('Mouse Out');
      $items['blur'] = t('Blur');
      $items['click'] = t('Click');
      $items['dbclick'] = t('Double Click');
      $items['hoverIntent'] = t('Hover Intent (options)');
    }

    if (!empty($values)) {
      foreach ($items as $key => $item) {
        if (in_array($key, $values)) {
          $items[$key] = $key;
        }
        else {
          $items[$key] = 0;
        }
      }
    }
    return $items;
  }
}
